import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import Chat from "./Chat";
const url = "https://edikdolynskyi.github.io/react_sources/messages.json";
ReactDOM.render(
  <React.StrictMode>
    <Chat url={url} />
  </React.StrictMode>,
  document.getElementById("root")
);
