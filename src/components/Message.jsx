import React, { useState } from "react";
import "../styles/message.css";
import ToggleIcon from "./ToggleIcon";
const Message = ({ item }) => {
  const [active, setActive] = useState(false);
  let [likes, setLikes] = useState(0);
  const handleChangeActive = (item) => {
    setActive((icon) => {
      return !icon;
    });
    item ? setLikes(likes + 1) : setLikes(likes - 1);
  };
  return (
    <>
      <div className="message">
        <div className="item">
          <img src={item.avatar} alt="avatar" className="message-user-avatar" />
          <span className="message-text">{item.text}</span>
        </div>{" "}
        <span>{item.user}</span>
        <div className="item">
          <span className="message-time">
            {new Date(item.createdAt).toLocaleString()}
          </span>
          <div className="message-like">
            <ToggleIcon
              active={active}
              handleChangeActive={handleChangeActive}
              likes={likes}
              iconActive={"fas"}
              iconInactive={"far"}
              icon={"heart"}
            ></ToggleIcon>
          </div>
        </div>
      </div>
    </>
  );
};

export default Message;
