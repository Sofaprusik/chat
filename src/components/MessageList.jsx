import React from "react";
import Message from "./Message";

const MessageList = ({ data }) => {
  return (
    <>
      {data.map((item) => (
        <>
          <Message item={item} key={item.id}></Message>
          <div>{item.time}</div>
        </>
      ))}
    </>
  );
};

export default MessageList;
